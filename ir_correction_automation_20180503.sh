#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

### 20 */4 * * * bash ${HOME}/ir_correction/v2/ir_correction_automation_20180503.sh > ${HOME}/ir_correction/v2/irc_automation_20180503_log 2>&1  

###############################
#### add (pub country) level and (country) level correction
###############################

start_time=$(date +%s)

###############################
####### for cron
PATH=$PATH:/usr/local/bin
source ${HOME}/aws_key.sh

##########################################################################################################################
##########################################################################################################################
##### Remove existing file in Google Cloud Storage
gsutil -m rm gs://adc_bigquery_exports/tx/irc_*_prod_v4.csv || true

echo "Success - Removed existing files in google cloud."

##########################################################################################################################
echo "Running BQ to download files ..."

#################################################
#### step 1: country level correction
#### table: tt_xu.irc_zone_country_prod_v4

#### big countries
bq rm -f -t tt_xu.irc_big_country_prod_v4
bq query --nouse_legacy_sql --replace=True --destination_table=tt_xu.irc_big_country_prod_v4 --allow_large_results \
"select country,  installs / greatest(expected_install,1) as correction
from
	(
		select lower(country) as country,
		sum(case when e.event_type = 'start' then 1 else 0 end) as impressions,
		sum(case when event_type in ('attributed_install_direct','attributed_install_indirect') then 1 else 0 end) as installs,
		sum(case when event_type = 'start' then adjusted_probability else 0 end) as expected_install
		from adcolony.events e
		inner join adcolony.video_zones z 
		on e.zone_id = z.id 
		where e._PARTITIONTIME >= TIMESTAMP(DATE_ADD(CURRENT_DATE(), INTERVAL -15 DAY)) 
		and e.recorded_time >= TIMESTAMP(DATE_ADD(CURRENT_DATE(), INTERVAL -14 DAY)) 
		and e.recorded_time < TIMESTAMP(CURRENT_DATE()) 
		and ( (e.event_type = 'start' and e.adjusted_probability is not null) or (e.event_type in ('attributed_install_direct','attributed_install_indirect')) )
		and e.yo_model in ('metamodel:v2.1:ircorrection', 'metamodel:v2.1:irfloorcorrection', 'metamodel:v2.1:20180201', 'metamodel:v2.1:brandsuppression90', 'metamodel:v2.1:brandsuppression50', 'metamodel:v2.1:discountcontrol')
		and z.test_ads = false and z.active = true
		and e.country is not null
		group by 1
		having impressions >= 10000 and installs >= 20
	)"

echo "Query Success - 1.1) tt_xu.irc_big_country_prod_v4" 

########################## add small country - 'other'
bq rm -f -t tt_xu.irc_country_prod_v4
bq query --nouse_legacy_sql --replace=True --destination_table=tt_xu.irc_country_prod_v4 --allow_large_results \
"select 'other' as country, greatest(installs,1) / greatest(expected_install,1) as correction
from
	(
		select 
		sum(case when e.event_type = 'start' then 1 else 0 end) as impressions,
		sum(case when event_type in ('attributed_install_direct','attributed_install_indirect') then 1 else 0 end) as installs,
		sum(case when event_type = 'start' then adjusted_probability else 0 end) as expected_install
		from adcolony.events e
		inner join adcolony.video_zones z 
		on e.zone_id = z.id 
		where e._PARTITIONTIME >= TIMESTAMP(DATE_ADD(CURRENT_DATE(), INTERVAL -15 DAY)) 
		and e.recorded_time >= TIMESTAMP(DATE_ADD(CURRENT_DATE(), INTERVAL -14 DAY)) 
		and e.recorded_time < TIMESTAMP(CURRENT_DATE()) 
		and ( (e.event_type = 'start' and e.adjusted_probability is not null) or (e.event_type in ('attributed_install_direct','attributed_install_indirect')) )
		and e.yo_model in ('metamodel:v2.1:ircorrection', 'metamodel:v2.1:irfloorcorrection', 'metamodel:v2.1:20180201', 'metamodel:v2.1:brandsuppression90', 'metamodel:v2.1:brandsuppression50', 'metamodel:v2.1:discountcontrol')
		and z.test_ads = false and z.active = true
		and e.country is not null
		and not exists (select * from tt_xu.irc_big_country_prod_v4 b where b.country = lower(e.country))
	)
union all 
select * from tt_xu.irc_big_country_prod_v4"

echo "Query Success - 1.2) tt_xu.irc_country_prod_v4" 


bq extract tt_xu.irc_country_prod_v4 gs://adc_bigquery_exports/tx/irc_country_prod_v4.csv
echo "Success - Extract query table to google cloud"


#################################################
#################################################
##### step 2: (publisher, country) level correction
### table: tt_xu.irc_pub_country_prod_v4
bq rm -f -t tt_xu.irc_pub_country_prod_v4
bq query --nouse_legacy_sql --replace=True --destination_table=tt_xu.irc_pub_country_prod_v4 --allow_large_results \
"select pub_id, country,
case when installs < 20 and expected_install > installs then (greatest(installs,1) / greatest(expected_install,1) + 0.6) / 2
	 when installs < 20 and expected_install < installs then (greatest(installs,1) / greatest(expected_install,1) + 1) / 2
	 else greatest(installs,1) / greatest(expected_install,1)
end as correction
from
	(
		select a.user_id as pub_id, lower(e.country) as country,
		sum(case when e.event_type = 'start' then 1 else 0 end) as impressions,
		sum(case when event_type in ('attributed_install_direct','attributed_install_indirect') then 1 else 0 end) as installs,
		sum(case when event_type = 'start' then adjusted_probability else 0 end) as expected_install
		from adcolony.events e
		inner join adcolony.video_zones z 
		on e.zone_id = z.id 
		inner join adcolony.apps a 
		on e.app_id = a.id
		where e._PARTITIONTIME >= TIMESTAMP(DATE_ADD(CURRENT_DATE(), INTERVAL -15 DAY)) 
		and e.recorded_time >= TIMESTAMP(DATE_ADD(CURRENT_DATE(), INTERVAL -14 DAY)) 
		and e.recorded_time < TIMESTAMP(CURRENT_DATE()) 
		and ( (e.event_type = 'start' and e.adjusted_probability is not null) or (e.event_type in ('attributed_install_direct','attributed_install_indirect')) )
		and e.yo_model in ('metamodel:v2.1:ircorrection', 'metamodel:v2.1:irfloorcorrection', 'metamodel:v2.1:20180201', 'metamodel:v2.1:brandsuppression90', 'metamodel:v2.1:brandsuppression50', 'metamodel:v2.1:discountcontrol')
		and z.test_ads = false and z.active = true
		and e.country is not null
		group by 1,2
		having impressions >= 20000
	)"

echo "Query Success - 2) tt_xu.irc_pub_country_prod_v4" 

bq extract tt_xu.irc_pub_country_prod_v4 gs://adc_bigquery_exports/tx/irc_pub_country_prod_v4.csv
echo "Success - Extract query table to google cloud"


#################################################
#################################################
### step 3: find high volume (zone, country) that has enough data for correction
### table: tt_xu.irc_zone_country_prod_v4
bq rm -f -t tt_xu.irc_zone_country_prod_v4
bq query --nouse_legacy_sql --replace=True --destination_table=tt_xu.irc_zone_country_prod_v4 --allow_large_results \
"select zone_id, country,
case when installs < 20 and expected_install > installs then (greatest(installs,1) / greatest(expected_install,1) + 0.6) / 2
	 when installs < 20 and expected_install < installs then (greatest(installs,1) / greatest(expected_install,1) + 1) / 2
	 else greatest(installs,1) / greatest(expected_install,1)
end as correction
from
	(
		select e.zone_id, lower(e.country)country,
		sum(case when e.event_type = 'start' then 1 else 0 end) as impressions,
		sum(case when event_type in ('attributed_install_direct','attributed_install_indirect') then 1 else 0 end) as installs,
		sum(case when event_type = 'start' then adjusted_probability else 0 end) as expected_install
		from adcolony.events e
		inner join adcolony.video_zones z 
		on e.zone_id = z.id 
		where e._PARTITIONTIME >= TIMESTAMP(DATE_ADD(CURRENT_DATE(), INTERVAL -15 DAY)) 
		and e.recorded_time >= TIMESTAMP(DATE_ADD(CURRENT_DATE(), INTERVAL -14 DAY)) 
		and e.recorded_time < TIMESTAMP(CURRENT_DATE()) 
		and ( (e.event_type = 'start' and e.adjusted_probability is not null) or (e.event_type in ('attributed_install_direct','attributed_install_indirect')) )
		and e.yo_model in ('metamodel:v2.1:ircorrection', 'metamodel:v2.1:irfloorcorrection', 'metamodel:v2.1:20180201', 'metamodel:v2.1:brandsuppression90', 'metamodel:v2.1:brandsuppression50', 'metamodel:v2.1:discountcontrol')
		and z.test_ads = false and z.active = true
		and e.country is not null
		group by 1,2
		having impressions >= 20000
	)"

echo "Query Success - 3) tt_xu.irc_zone_country_prod_v4" 



#################################################
#################################################
### step 4: get DAILY imp, ins, expected_install for all ads in the above high volume (zone, country) 
### table: tt_xu.irc_zone_country_all_ads_daily_prod_v4
bq rm -f -t tt_xu.irc_zone_country_all_ads_daily_prod_v4
bq query --nouse_legacy_sql --replace=True --destination_table=tt_xu.irc_zone_country_all_ads_daily_prod_v4 --allow_large_results \
"select e.zone_id, lower(e.country)country, e.ad_id, 
DATE_DIFF(date(recorded_time), date(ads.created_at), DAY) as ad_age,
sum(case when e.event_type = 'start' then 1 else 0 end) as impressions,
sum(case when event_type in ('attributed_install_direct','attributed_install_indirect') then 1 else 0 end) as installs,
sum(case when event_type = 'start' then adjusted_probability else 0 end) as expected_install
from adcolony.events e
inner join adcolony.ads ads 
on e.ad_id = ads.id 
where e._PARTITIONTIME >= TIMESTAMP(DATE_ADD(CURRENT_DATE(), INTERVAL -15 DAY)) 
and e.recorded_time >= TIMESTAMP(DATE_ADD(CURRENT_DATE(), INTERVAL -14 DAY)) 
and e.recorded_time < TIMESTAMP(CURRENT_DATE()) 
and ( (e.event_type = 'start' and e.adjusted_probability is not null) or (e.event_type in ('attributed_install_direct','attributed_install_indirect')) )
and e.yo_model in ('metamodel:v2.1:ircorrection', 'metamodel:v2.1:irfloorcorrection', 'metamodel:v2.1:20180201', 'metamodel:v2.1:brandsuppression90', 'metamodel:v2.1:brandsuppression50', 'metamodel:v2.1:discountcontrol')
and exists (select 1 from tt_xu.irc_zone_country_prod_v4 a where e.zone_id = a.zone_id and lower(e.country) = a.country)
group by 1,2,3,4"

echo "Query Success - 4) tt_xu.irc_zone_country_all_ads_daily_prod_v4" 

#################################################
#################################################
#### step 5: get sum imp, ins, expected_install for all old ads in the above high volume (zone, country) 
#### remove data when ad_age < 4. ad_age = date_diff(recorded_time - ads.created_at)
bq rm -f -t tt_xu.irc_zone_country_all_ads_prod_v4
bq query --nouse_legacy_sql --replace=True --destination_table=tt_xu.irc_zone_country_all_ads_prod_v4 --allow_large_results \
"select zone_id, country, ad_id,
sum(impressions) as impressions, sum(installs) as installs, sum(expected_install) as expected_install
from tt_xu.irc_zone_country_all_ads_daily_prod_v4
where ad_age > 3
group by 1,2,3"

echo "Query Success - 5): tt_xu.irc_zone_country_all_ads_prod_v4"

#################################################
#################################################
#### step 6: find high volume (zone, country, ad) and get a correction coefficient directly
#### for old ads
bq rm -f -t tt_xu.irc_zone_country_ad_temp_prod_v4
bq query --nouse_legacy_sql --replace=True --destination_table=tt_xu.irc_zone_country_ad_temp_prod_v4 --allow_large_results \
"select zone_id, country, ad_id,
case when installs < 20 and expected_install > installs then (greatest(installs,1) / greatest(expected_install,1) + 0.6) / 2  -- overestimate
	 when installs < 20 and expected_install < installs then (greatest(installs,1) / greatest(expected_install,1) + 1) / 2  -- underestimate
	 else greatest(installs,1) / greatest(expected_install,1)
end as correction
from tt_xu.irc_zone_country_all_ads_prod_v4
where impressions * greatest(installs, expected_install) >= 100000
order by installs "

echo "Query Success - 6): tt_xu.irc_zone_country_ad_temp_prod_v4" 

#################################################
#################################################
#### step 7: remove high volume (zone, country, ad) for each high volume (zone, country) and compute zone-level correction
bq rm -f -t tt_xu.irc_zone_country_ex_prod_v4
bq query --nouse_legacy_sql --replace=True --destination_table=tt_xu.irc_zone_country_ex_prod_v4 --allow_large_results \
"select zone_id, country,
case when total_installs < 20 and total_expected_install > total_installs then (greatest(total_installs,1) / greatest(total_expected_install,1) + 0.6) / 2  -- overestimate
	 when total_installs < 20 and total_expected_install < total_installs then (greatest(total_installs,1) / greatest(total_expected_install,1) + 1) / 2  -- underestimate
	 else greatest(total_installs,1) / greatest(total_expected_install,1)
end as correction
from
	(
		select zone_id, country, 
		sum(impressions) as total_impressions, sum(installs) as total_installs, sum(expected_install) as total_expected_install
		from tt_xu.irc_zone_country_all_ads_prod_v4
		where impressions * greatest(installs, expected_install) < 100000
		group by 1,2
		having total_impressions * greatest(total_installs, total_expected_install) >= 100000
	)"

echo "Query Success - 7): tt_xu.irc_zone_country_ex_prod_v4" 

#################################################
#################################################
#### step 8:  new ua ads
bq rm -f -t tt_xu.irc_new_ua_ads_prod_v4
bq query --nouse_legacy_sql --replace=True --destination_table=tt_xu.irc_new_ua_ads_prod_v4 --allow_large_results \
"select distinct ads.id as ad_id 
from adcolony.ads ads
inner join adcolony.ad_group_manifests agm
on ads.id = agm.ad_id
inner join adcolony.ad_campaigns ac
on agm.ad_campaign_id = ac.id
where ads.created_at >= TIMESTAMP(DATE_ADD(CURRENT_DATE(), INTERVAL -3 DAY)) 
and ac.type = 'app_install'"

echo "Query Success - 8): tt_xu.irc_new_ua_ads_prod_v4"

bq extract tt_xu.irc_new_ua_ads_prod_v4 gs://adc_bigquery_exports/tx/irc_new_ua_ads_prod_v4.csv
echo "Success - Extract query table to google cloud"

#################################################
#################################################
#### step 9: get correction for high volume new ads in the high volume (zone, country), add to the ad table
bq rm -f -t tt_xu.irc_zone_country_ad_prod_v4
bq query --nouse_legacy_sql --replace=True --destination_table=tt_xu.irc_zone_country_ad_prod_v4 --allow_large_results \
"select * from tt_xu.irc_zone_country_ad_temp_prod_v4
union all 
select * from
	(
		select zone_id, country, ad_id,
		greatest(installs,1) / greatest(expected_install,1) as correction
		from 
			(
				select zone_id, country, t1.ad_id, 
				sum(impressions) as impressions, sum(installs) as installs, sum(expected_install) as expected_install
				from tt_xu.irc_zone_country_all_ads_daily_prod_v4 t1
				inner join tt_xu.irc_new_ua_ads_prod_v4 t2 
				on t1.ad_id = t2.ad_id 
				where ad_age <= 3
				group by 1,2,3
				having (impressions * greatest(installs, expected_install) >= 50000)
			)
	)"

echo "Query Success - 9): tt_xu.irc_zone_country_ad_prod_v4"

bq extract tt_xu.irc_zone_country_ad_prod_v4 gs://adc_bigquery_exports/tx/irc_zone_country_ad_prod_v4.csv
echo "Success - Extract query table to google cloud"

#################################################
#################################################
### step 10: merge irc_zone_country_prod_v4 and irc_zone_country_ex_prod_v4
bq rm -f -t tt_xu.irc_zone_country_merge_prod_v4
bq query --nouse_legacy_sql --replace=True --destination_table=tt_xu.irc_zone_country_merge_prod_v4 --allow_large_results \
"select t1.zone_id, t1.country, t1.correction as correction, t2.correction as correction_ex 
from tt_xu.irc_zone_country_prod_v4 t1 
left join tt_xu.irc_zone_country_ex_prod_v4 t2
on t1.zone_id = t2.zone_id and t1.country = t2.country"

echo "Query Success - 10) tt_xu.irc_zone_country_merge_prod_v4" 

bq extract tt_xu.irc_zone_country_merge_prod_v4 gs://adc_bigquery_exports/tx/irc_zone_country_merge_prod_v4.csv
echo "Success - Extract query table to google cloud"


##########################################################################################################################
#### upload tables from gcloud to S3
gsutil cp gs://adc_bigquery_exports/tx/irc_country_prod_v4.csv s3://adcolony-data-science/projects/ir_correction/v4/irc_country_prod.csv
gsutil cp gs://adc_bigquery_exports/tx/irc_pub_country_prod_v4.csv s3://adcolony-data-science/projects/ir_correction/v4/irc_pub_country_prod.csv
gsutil cp gs://adc_bigquery_exports/tx/irc_zone_country_merge_prod_v4.csv s3://adcolony-data-science/projects/ir_correction/v4/irc_zone_country_prod.csv
gsutil cp gs://adc_bigquery_exports/tx/irc_zone_country_ad_prod_v4.csv s3://adcolony-data-science/projects/ir_correction/v4/irc_zone_country_ad_prod.csv
gsutil cp gs://adc_bigquery_exports/tx/irc_new_ua_ads_prod_v4.csv s3://adcolony-data-science/projects/ir_correction/v4/irc_new_ua_ads_prod.csv

echo "Success - Uploaded results to S3."

##########################################################################################################################
##########################################################################################################################
end_time=$(date +%s)
time_diff=$(($end_time - $start_time))
echo "Total time elapsed: $(($time_diff/60)) minutes."  ## 4min

